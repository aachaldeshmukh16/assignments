package Streams;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Remaining {

        public static void main(String[] args) {
            List<Integer> list = Arrays.asList(2,7,5,6,9,0,9);

            Optional<Integer> maxNumber = list.stream().max(Integer::compareTo);
            System.out.println("max" + maxNumber);
            maxNumber.ifPresent(System.out::println);

            Optional<Integer> minNumber = list.stream().min(Integer::compareTo);
            System.out.println("min" + minNumber);
            minNumber.ifPresent(System.out::println);

            //to count the quantity of that number
            long count = list.stream().filter(i -> i==9).count();
            System.out.println(count);
            System.out.println(list.stream().filter(i -> i==9).count());

            System.out.println("AAAAAAAA");
            //Square Without duplicates:
            System.out.println(list.stream().distinct().map(i ->i*i).collect(Collectors.toList()));

            //to remove duplicates
            System.out.println(list.stream().distinct().collect(Collectors.toList()));

            //to sort
            System.out.println(list.stream().sorted().collect(Collectors.toList()));


            //match to return present or not
            // List<Integer> integers = Arrays.asList()


            //skip
            System.out.println(list.stream().skip(2).collect(Collectors.toList()));

        }

    }


