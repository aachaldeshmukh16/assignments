package Streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamsAssignment {

    public List<String> validation(List<String> first){
        List<String> result = first.stream().filter(i -> i.length()>2).collect(Collectors.toList());
        return result;
    }

    public List<String> convert(List<String> first1){
        List<String> result = first1.stream().map(String::toUpperCase).collect(Collectors.toList());
        return result;

    }

    public long count(List<Integer> list){
        long result = list.stream().filter(i -> i==2).count();
        return result;
    }

    public List<Integer> squareRemoveDuplicates(List<Integer> list1){
        List<Integer>result = list1.stream().distinct().map(i ->i*i).collect(Collectors.toList());
        return result;
    }



    public static void main(String[] args) {

        //Filter out list of strings where length of string length less than 2
        List<String> first = Arrays.asList("abc", "bcd", "dfeg", "ik");
        StreamsAssignment filter = new StreamsAssignment();
        System.out.println("Filter out list of strings where length of string length less than 2");
        System.out.println(filter.validation(first));
        System.out.println("");

        //Convert String to Uppercase and join them using coma
        List<String> first1 = Arrays.asList("USA", "Japan", "France", "Germany", "India", "U.K", "Canada");
        StreamsAssignment uppercase = new StreamsAssignment();
        System.out.println("String to Uppercase and join them using coma");
        System.out.println(uppercase.convert(first1));
        System.out.println("");

        //Count number of 2’s in the list
        List<Integer> list = Arrays.asList(1,2,2,4,2,5);
        StreamsAssignment count = new StreamsAssignment();
        System.out.println("Number of 2’s in the list");
        System.out.println(filter.count(list));
        System.out.println("");

        //Square Without duplicates:
        List<Integer> list1 = Arrays.asList(9, 10, 3, 4, 7, 3, 4);
        StreamsAssignment square = new StreamsAssignment();
        System.out.println("Square Without duplicates");
        System.out.println(filter.squareRemoveDuplicates(list1));

    }


}