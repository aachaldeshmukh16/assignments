package MatrixProblems;

public class MatrixExamples {
    public static int[][] add(int[][] a, int[][] b) {
        int rows = a.length;
        int columns = a[0].length;
        int[][] result = new int[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                result[i][j] = a[i][j] + b[i][j];
            }
        }
        return result;
    }

    public static int[][] sub(int[][] a, int[][] b) {
        int rows = a.length;
        int columns = a[0].length;
        int[][] result = new int[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                result[i][j] = a[i][j] - b[i][j];
            }
        }
        return result;
    }

    public static int[][] mul(int[][] a, int[][] b) {
        int rows = a.length;
        int columns = a[0].length;
        int[][] result = new int[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                result[i][j] = a[i][j] * b[i][j];
            }
        }
        return result;
    }



    public static void PrintMatrix(int[][] a) {
        int rows = a.length;
        int columns = a[0].length;
        int[][] result = new int[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                System.out.print(a[i][j]);
            }
            System.out.println();
        }

    }

    public static void main(String[] args){
        int[][] a = {{1,2,3},{1,2,3}};
        int[][] b = {{1,2,3},{1,2,3}};

        System.out.println("Addition");
        int[][] c = add(a,b);
        PrintMatrix(c);
        System.out.println("");

        System.out.println("Substraction");
        int[][] d = sub(a,b);
        PrintMatrix(d);
        System.out.println("");

        System.out.println("Multiplication");
        int[][] e = mul(a,b);
        PrintMatrix(e);






    }
}
