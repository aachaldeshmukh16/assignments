package Basics;

public class Factorial {
    static int factorial(int num)
    {
        int factorial = 1, i;
        for (i=2; i<=num; i++)
            factorial = factorial*i;
        return factorial;
    }


    public static void main(String[] args)
    {
        int num = 5;
        System.out.println("Factorial of "+ num + " is " + factorial(num));
    }

}
