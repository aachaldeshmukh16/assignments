package Basics;

public class Palindrom {
        static int reverseDigits(int num)
        {
            int reverse = 0;
            while (num > 0) {
                reverse = reverse * 10 + num % 10;
                num = num / 10;
            }
            return reverse;
        }
        static int isPalindrome(int n)
        {
            int rev_n = reverseDigits(n);
            if (rev_n == n)
                return 1;
            else
                return 0;
        }


        public static void main(String []args)
        {
            int n = 1234321;
            System.out.println( n +" " + "is a Palindrome number" +":" + (isPalindrome(n) == 1 ? "true" : "false"));

        }

    }



